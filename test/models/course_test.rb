require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:one)
    @user.save
    
    @location = Location.new(name: "14.10.23")
    @location.save
    
    @category = Category.new(name: "iOS Development")
    @category.save
    
    @course = Course.new(
      name: "Sample Course",
      prerequisite: "Advanced Programming Techniques",
      created_by: @user.id,
      description: "Sample description for this sample course",
      locations: [@location],
      categories: [@category]
    )
  end
  
  test "should be valid" do
    assert @course.valid?
  end
  
  test "prerequisite must meet minimum length" do
    @course.prerequisite = ""
    assert_not @course.valid?
  end
  
  test "locations and categories must meet minimum length" do
    @course.locations = []
    @course.categories = []
    assert_not @course.valid?
  end
end
