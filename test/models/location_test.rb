require 'test_helper'

class LocationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @loc = Location.new(name: "14.10.31")
  end
  
  test "should be valid" do
    assert @loc.valid?
  end
  
  test "name should be unique" do
    duplicate_loc = @loc.dup
    duplicate_loc.name = "14.010.31"
    @loc.save
    assert_not duplicate_loc.valid?
  end
end
