require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @cat = Category.new(name: "iOS Development")
  end
  
  test "should be valid" do
    assert @cat.valid?
  end
  
  test "name should be unique" do
    duplicate_cat = @cat.dup
    duplicate_cat.name = @cat.name.upcase
    @cat.save
    assert_not duplicate_cat.valid?
  end
end
