require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:one)
    @user.save
    
    @course = courses(:one)
    @course.save
    
    @vote = Vote.new(
      "vote_type" => 1, 
      "course_id" => @course.id, 
      "user_id" => @user.id
    )
  end
  
  test "should be valid" do
    assert @vote.valid?
  end
end
