class Course < ApplicationRecord
    has_and_belongs_to_many :categories
    has_and_belongs_to_many :locations
    has_one :user, :class_name => "User", :foreign_key => "id", :primary_key => "created_by"
    has_many :votes
    
    mount_uploader :imgtemp, ImageUploader
    
    validates :name, presence: true, length: { minimum: 10, maximum: 255 }
    
    validates :prerequisite, presence: true, length: { minimum: 10, maximum: 255 }
    
    validates :description, presence: true, length: { minimum: 10 }
    
    validate :atleast_one_category_selected
    
    validate :atleast_one_location_selected
    
    def atleast_one_category_selected
    if self.category_ids.blank?
      self.errors.add(:category_ids, "need atleast one selection")
    end
    end
    
    def atleast_one_location_selected
    if self.location_ids.blank?
      self.errors.add(:location_ids, "need atleast one selection")
    end
    end
    
    HUMANIZED_ATTRIBUTES = {
    :category_ids => "Category",
    :location_ids => "Location",
    }

    def self.human_attribute_name(attr, options = {})
        HUMANIZED_ATTRIBUTES[attr.to_sym] || super
    end
    
    def votes_of_type(type)
      @votes = self.votes.where(["vote_type = :type", {type: type}])
    end
    
end
