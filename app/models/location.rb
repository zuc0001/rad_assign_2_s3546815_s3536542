class Location < ApplicationRecord
    has_and_belongs_to_many :courses
    
    validate :remove_zeros
    
    VALID_LOCATION_REGEX = /\A([0-9]{1,3}[.]){2}[0-9]{1,3}\Z/
    validates :name, presence: true, format: {with: VALID_LOCATION_REGEX,
                                        :message => "must be in 0.0.0 format"},
                                        uniqueness: {:message => "\"%{value}\" already in the system"}
    
    HUMANIZED_ATTRIBUTES = {
        :name => "Location"
    }
    
    def self.human_attribute_name(attr, options = {})
        HUMANIZED_ATTRIBUTES[attr.to_sym] || super
    end
    
    def remove_zeros
        temp = self.name.sub!(/^[0]{1,}/, "")
        if(temp != nil)
            self.name = temp
        end
        temp = self.name.gsub!(/[.]([0]{1,})/, ".")
        if(temp != nil) 
            self.name = temp
        end
    end
end
 