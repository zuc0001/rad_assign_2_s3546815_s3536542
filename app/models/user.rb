class User < ApplicationRecord
    validates :name, presence: true, length: {minimum:4, maximum: 125}, :if => lambda {|u| !u.admin?}
    
    VALID_EMAIL_REGEX = /\A([A-Za-z]{1,})[.]([A-Za-z]{1,})(@rmit.edu.au)\z/i
    validates :email, presence: true, length: {minimum:4, maximum: 255}, 
        format: {with: VALID_EMAIL_REGEX, 
            :message => "Registration is only open for RMIT staff"}, 
        uniqueness: { case_sensitive: false }, :if => lambda {|u| !u.admin?}
    
    VALID_PASSWORD_REGEX = /\A(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,}\z/
    validates :password, presence: true, length: {minimum: 8}, 
        format: {with: VALID_PASSWORD_REGEX, 
            :message => "must contain at least a lowercase letter, a digit, a special character and at least 8 characters."}, :if => lambda {|u| !u.admin?}
            
                    
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
    
    def User.new_token
        SecureRandom.urlsafe_base64
    end
    
    attr_accessor :remember_token
    
    def remember
        self.remember_token = User.new_token
        update_attribute(:remember_digest, User.digest(remember_token))
    end
    
    def authenticated?(remember_token)
        return false if remember_digest.nil?
        BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end
    
    def forget
        update_attribute(:remember_digest, nil)
    end
    
    before_save {self.email = email.downcase}
    has_secure_password
end