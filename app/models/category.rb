class Category < ApplicationRecord
    has_and_belongs_to_many :courses
    
     HUMANIZED_ATTRIBUTES = {
        :name => "Category"
    }
    
    def self.human_attribute_name(attr, options = {})
        HUMANIZED_ATTRIBUTES[attr.to_sym] || super
    end
    
    validates :name, presence: true, length: { maximum: 255 },
                                        uniqueness: { case_sensitive: false,
                                            :message => "\"%{value}\" already added in the system"}
    
end
