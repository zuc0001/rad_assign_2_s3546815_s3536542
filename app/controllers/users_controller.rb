class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
  before_action :is_admin, only: [:destroy, :index]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @courses = Course.where("created_by = :id", {id: @user.id}).all()
  end

  # GET /users/new
  def new
    if logged_in? 
      redirect_to root_url
    end
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    if @user.admin
      redirect_to users_path
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      log_in @user
      redirect_back_or
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        flash[:success] = "User was successfully updated."
        format.html { redirect_to @user }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      flash[:success] = "User was successfully destroyed."
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
    
    def correct_user
      @user = User.find(params[:id])
      if(!current_user?(@user))
        flash[:danger] = "Please log in with correct user to access that specific user"
        redirect_to root_url;
      end
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
end
