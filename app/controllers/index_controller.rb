class IndexController < ApplicationController
    
    def index
        if(logged_in?)
            redirect_to controller: 'courses', action: 'index'
        end
    end
end
