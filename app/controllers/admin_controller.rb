class AdminController < ApplicationController
    before_action :is_admin, only: [:panel]

  def login
      if(logged_in?)
         redirect_to admin_panel_path 
      end
  end
  
  def loginPost
    if(params[:admin][:username].downcase == "admin" && params[:admin][:password].downcase == "password")
        _user = User.new(id: -1, admin: true, name: "Administrator")
        log_in _user
        redirect_to root_path
    else
        flash[:danger] = "Incorrect username and/or password."
        render 'login'
    end
  end
  
  def panel
  end
  
end
