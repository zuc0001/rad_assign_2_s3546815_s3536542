class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionsHelper
  
  before_action :inject_components
  def inject_components
      @categories = Category::all
      @locations = Location::all
  end
  
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end
  
  def is_admin
    redirect_to login_path if current_user.nil? or !current_user.admin?
  end
  
  
end
