json.array!(@courses) do |course|
  json.extract! course, :id, :name, :prerequisite, :created_by
  json.url course_url(course, format: :json)
end
