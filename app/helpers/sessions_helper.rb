module SessionsHelper
    def log_in(user)
       flash[:info] = "Hi #{user.name}. You've successfully logged in!"
       session[:user_id] = user.id 
       if(user.admin?)
          session[:admin] = true 
       end
    end
    
    def remember(user)
        user.remember
        cookies.permanent.signed[:user_id] = user.id
        cookies.permanent[:remember_token] = user.remember_token
    end
    
    def current_user
        if (user_id = session[:user_id])
            @current_user ||= User.find_by(id: user_id)
            if(user_id == -1 && !session[:admin].nil? && @current_user.nil?) 
               @current_user = User.create(id: -1, name: "Administrator", email: "admin@rmit.edu.au", admin: true, password: "password") 
            else
               current_user = @current_user
            end
        elsif (user_id = cookies.signed[:user_id])
            user = User.find_by(id: user_id)
            if user && user.authenticated?(cookies[:remember_token])
                log_in user
                @current_user = user
            end
        end
    end
    
    def current_user?(user)
        user == current_user
    end
    
    def logged_in?
        !current_user.nil?
    end
    
    def forget(user)
        user.forget
        cookies.delete(:user_id)
        cookies.delete(:remember_token)
        cookies.delete(:admin)
    end
    
    def log_out
        flash[:success] = "You've successfully logged out!"
        forget(current_user)
        session.delete(:user_id)
        cookies.delete(:admin)
        @current_user = nil
    end
    
    # Redirects to stored location (or to the default).
    def redirect_back_or
        store_location
        redirect_to session[:forwarding_url]
        session.delete(:forwarding_url)
    end
    
    # Stores the URL trying to be accessed.
    def store_location
        session[:forwarding_url] = request.original_url || default
    end
end
