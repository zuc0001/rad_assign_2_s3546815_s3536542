Hi :)

Development Team:
- Nicholas Zuccarelli (s3546815)
- Nivin Goonesekera (s3536542)


Heroku Deployment URL:
- https://quiet-hollows-17916.herokuapp.com/

  
Additional Gems:
- For image uploads (updated spec), we used a gem called "Imgur-API"
  which uploads images to Imgur
    Unfortunately, the GEM is 3 years old so we had to made some changes
    to the gem module code itself. 
    To do this, we have put the gem code at the top of "CourseController".

- Added "minihelper" gem because of an error that was appearing when
  running test cases.
    https://github.com/seattlerb/minitest/issues/689
    The issue was documented here and a fix was to use the previous
    minitest gem version.

    




DEPLOYMENT LOG FOR HEROKU:
---------------------------------
-----> Ruby app detected
-----> Compiling Ruby/Rails
-----> Using Ruby version: ruby-2.3.4
-----> Installing dependencies using bundler 1.13.7
       Running: bundle install --without development:test --path vendor/bundle --binstubs vendor/bundle/bin -j4 --deployment
       Warning: the running version of Bundler (1.13.7) is older than the version that created the lockfile (1.14.6). We suggest you upgrade to the latest version of Bundler by running `gem install bundler`.
       Fetching gem metadata from https://rubygems.org/...........
       Fetching version metadata from https://rubygems.org/..
       Fetching dependency metadata from https://rubygems.org/.
       Using rake 12.0.0
       Using concurrent-ruby 1.0.5
       Using i18n 0.8.1
       Using thread_safe 0.3.6
       Using builder 3.2.3
       Using erubis 2.7.0
       Using mini_portile2 2.1.0
       Using nio4r 1.2.1
       Using websocket-extensions 0.1.2
       Using mime-types-data 3.2016.0521
       Using arel 7.1.4
       Using execjs 2.7.0
       Using bcrypt 3.1.11
       Installing minitest 5.10.2
       Using coffee-script-source 1.12.2
       Using method_source 0.8.2
       Using thor 0.19.4
       Installing rack 2.0.3
       Installing multi_xml 0.6.0
       Installing sass 3.4.24
       Using multi_json 1.12.1
       Using mini_magick 4.7.0
       Using pg 0.18.4
       Using puma 3.4.0
       Using bundler 1.13.7
       Using tilt 2.0.7
       Installing turbolinks-source 5.0.3
       Using tzinfo 1.2.3
       Using websocket-driver 0.6.5
       Using mime-types 3.1
       Installing autoprefixer-rails 7.1.1
       Using uglifier 3.0.0
       Using coffee-script 2.4.1
       Installing httparty 0.15.5
       Using turbolinks 5.0.1
       Using activesupport 5.0.1
       Using rack-test 0.6.3
       Using sprockets 3.7.1
       Installing mail 2.6.5
       Using bootstrap-sass 3.3.6
       Installing globalid 0.4.0
       Using activemodel 5.0.1
       Using jbuilder 2.4.1
       Installing nokogiri 1.7.2 with native extensions
       Installing imgur-api 0.0.4
       Using activejob 5.0.1
       Using activerecord 5.0.1
       Installing carrierwave 1.1.0
       Using loofah 2.0.3
       Using rails-html-sanitizer 1.0.3
       Installing rails-dom-testing 2.0.3
       Using actionview 5.0.1
       Using actionpack 5.0.1
       Using actioncable 5.0.1
       Using actionmailer 5.0.1
       Using railties 5.0.1
       Using sprockets-rails 3.2.0
       Using coffee-rails 4.2.1
       Using jquery-rails 4.1.1
       Using rails 5.0.1
       Using sass-rails 5.0.6
       Installing font-awesome-rails 4.7.0.2
       Bundle complete! 21 Gemfile dependencies, 62 gems now installed.
       Gems in the groups development and test were not installed.
       Bundled gems are installed into ./vendor/bundle.
       Post-install message from httparty:
       When you HTTParty, you must party hard!
       Bundle completed (10.94s)
       Cleaning up the bundler cache.
       Warning: the running version of Bundler (1.13.7) is older than the version that created the lockfile (1.14.6). We suggest you upgrade to the latest version of Bundler by running `gem install bundler`.
       Removing carrierwave (1.0.0)
       Removing autoprefixer-rails (6.7.7.1)
       Removing turbolinks-source (5.0.0)
       Removing rails-dom-testing (2.0.2)
       Removing globalid (0.3.7)
       Removing font-awesome-rails (4.7.0.1)
       Removing minitest (5.10.1)
       Removing sass (3.4.23)
       Removing nokogiri (1.7.1)
       Removing rack (2.0.1)
       Removing mail (2.6.4)
-----> Installing node-v6.10.0-linux-x64
-----> Detecting rake tasks
-----> Preparing app for Rails asset pipeline
       Running: rake assets:precompile
       I, [2017-05-23T04:53:28.335042 #690]  INFO -- : Writing /tmp/build_b1424a449315bd8703245cde66440411/public/assets/application-25cb8676bc7fb424f58564ac2e559023ee893452f4fd70a8fcc4736c606a5f71.js
       I, [2017-05-23T04:53:28.335672 #690]  INFO -- : Writing /tmp/build_b1424a449315bd8703245cde66440411/public/assets/application-25cb8676bc7fb424f58564ac2e559023ee893452f4fd70a8fcc4736c606a5f71.js.gz
       Asset precompilation completed (16.12s)
       Cleaning assets
       Running: rake assets:clean
###### WARNING:
       You have not declared a Ruby version in your Gemfile.
       To set your Ruby version add this line to your Gemfile:
       ruby '2.3.4'
       # See https://devcenter.heroku.com/articles/ruby-versions for more information.
###### WARNING:
       No Procfile detected, using the default web server.
       We recommend explicitly declaring how to boot your server process via a Procfile.
       https://devcenter.heroku.com/articles/ruby-default-web-server
-----> Discovering process types
       Procfile declares types     -> (none)
       Default types for buildpack -> console, rake, web, worker
-----> Compressing...
       Done: 40.8M
-----> Launching...
       Released v6
       https://quiet-hollows-17916.herokuapp.com/ deployed to Heroku