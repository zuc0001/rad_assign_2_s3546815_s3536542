Rails.application.routes.draw do

  get 'errors/not_found'

  get 'errors/internal_server_error'

  root 'index#index'
  
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  
  get '/admin', to: 'admin#login'
  post '/admin', to: 'admin#loginPost'
  get '/admin/panel', to: 'admin#panel'
  
  post '/courses/vote', to: 'courses#vote'
  post '/courses/:id', to: 'courses#resetVotes'
  
  resources :locations
  resources :courses
  resources :categories
  resources :users
  
  #if Rails.env.production?
    #get '404', to: 'application#page_not_found'
    #get '422', to: 'application#server_error'
    #get '500', to:  'application#server_error'
  #end
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  
end
