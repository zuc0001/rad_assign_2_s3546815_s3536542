class AddDescriptionImgToCourses < ActiveRecord::Migration[5.0]
  def change
    add_column :courses, :description, :string
    add_column :courses, :img, :string
  end
end
