class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes, id: false do |t|
      t.integer :user_id
      t.integer :course_id
      t.integer :vote_type
      
      t.timestamps
    end
    add_index :votes, ["user_id", "course_id"], :unique => true
  end
end
