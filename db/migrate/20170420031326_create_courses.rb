class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :prerequisite
      t.integer :category_id
      t.integer :location_id
      t.integer :created_by

      t.timestamps
    end
  end
end
